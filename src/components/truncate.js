const truncateEnd = (str, maxLength) => {
  const hasToTruncate = str.length > maxLength;
  if (hasToTruncate) {
    return `${str.substr(0, maxLength - 3)}...`;
  }
  return str;
};

const truncateStart = (str, maxLength) => {
  const hasToTruncate = str.length > maxLength;
  if (hasToTruncate) {
    return `...${str.substr(str.length - maxLength + 3)}`;
  }
  return str;
};

const truncateSmart = (str, len) => {
  const hasToTruncate = str.length > len;
  if (!hasToTruncate) {
    return str;
  }
  const SEPARATOR = "/";
  const chunks = str.split(SEPARATOR);
  const rootFolder = chunks[0];
  const fileName = chunks[chunks.length - 1];
  const minimalLength = rootFolder.length + fileName.length;
  const remainingLength = len - minimalLength - 5; // ellipsis and slash

  if (remainingLength > 0) {
    chunks.splice(0, 1);
    chunks.splice(chunks.length - 1, 1);
    let path = chunks.join(SEPARATOR);
    const lenA = Math.ceil(remainingLength / 2);
    const lenB = Math.floor(remainingLength / 2);
    const pathA = path.substring(0, lenA);
    const pathB = path.substring(path.length - lenB);
    path = rootFolder + "/" + pathA + "..." + pathB + "/" + fileName;
    return path;
  } else {
    return truncateEnd(str, len);
  }
};

export const truncate = (str, maxLength, position = "end") => {
  switch (position) {
    case "end":
      return truncateEnd(str, maxLength);
    case "start":
      return truncateStart(str, maxLength);
    case "middle":
      return truncateSmart(str, maxLength);
  }
};
